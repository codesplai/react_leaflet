import React from "react";
import { Button, Input, InputGroup, InputGroupAddon, Alert } from "reactstrap";

const getAddressMap = textAddress => {
  return new Promise((resolve, reject) => {
    if (!textAddress) {
      resolve([]);
      reject({ error: "nodata" });
      return;
    }
    fetch(
      `https://us1.locationiq.com/v1/search.php?key=pk.b6318f65a0a482c1076929bb005aa0ae&q=${encodeURI(
        textAddress
      )}&format=json`
    )
      .then(data => data.json())
      .then(data => {
        if (data.error) {
          reject(data);
        } else {
          resolve(data);
        }
      })
      .catch(error => {
        reject({ error });
      });
  });
};

const AddressItem = props => {
  return (
    <Alert
      style={{ cursor: "pointer" }}
      color={
        props.current && props.current.place_id === props.item.place_id
          ? "danger"
          : "success"
      }
      onClick={() => props.onClick(props.item)}
    >
      {props.item.display_name}
    </Alert>
  );
};

const AddressList = props => {
  let i = 1000;
  return (
    <div className="address_list">
      {props.list.map(el => (
        <AddressItem
          key={i++}
          onClick={props.onClick}
          current={props.current}
          item={el}
        />
      ))}
    </div>
  );
};

export default class GeoLoc extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      address: "",
      resultAddress: [],
      // hasError: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.getAddress = this.getAddress.bind(this);
    this.removeAddress = this.removeAddress.bind(this);
    this._handleKeyDown = this._handleKeyDown.bind(this);
  }



    _handleKeyDown = (e) => {
      if (e.key === 'Enter') {
        this.getAddress();
      }
    }
  

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  removeAddress() {
    this.setState({ address: "", resultAddress: [] });
  }

  getAddress() {
    getAddressMap(this.state.address)
      .then(data => {
        this.setState({ resultAddress: data });
      })
      .catch(err => console.log(err));
  }

  render() {
    return (
      <>
      <h3>GeoLoc</h3>
      <hr />
        <InputGroup style={{ paddingBottom: "1em" }}>
          <Input
            type="text"
            name="address"
            id="address"
            value={this.state.address}
            onChange={this.handleChange}
            onKeyDown={this._handleKeyDown} 
          />
          <InputGroupAddon addonType="append">
            <Button color="danger" onClick={this.removeAddress}>
              <i className="fa fa-trash" aria-hidden="true"></i>
            </Button>
          </InputGroupAddon>
          <InputGroupAddon addonType="append">
            <Button color="primary" onClick={this.getAddress}>
              <i className="fa fa-search" aria-hidden="true"></i>
            </Button>
          </InputGroupAddon>
        </InputGroup>
        <AddressList
          onClick={this.props.setCurrent}
          list={this.state.resultAddress}
          current={this.props.current}
        />

      </>
    );
  }
}
